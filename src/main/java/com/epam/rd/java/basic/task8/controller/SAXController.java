package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.handlers.AdvancedXMLHandler;
import com.epam.rd.java.basic.task8.handlers.ObjectHandlerSax;
import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;
    private SAXParserFactory factory = SAXParserFactory.newInstance();
    private SAXParser parser;

    private static List<Flower> flowers = new ArrayList<>();

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse() throws ParserConfigurationException, SAXException, IOException {
        this.parser = factory.newSAXParser();

        ObjectHandlerSax handler = new ObjectHandlerSax();
        parser.parse(new File(xmlFileName), handler);

        flowers = handler.getResult();

        /*
        for (Flower flower : flowers)
            System.out.println(flower);
*/
    }

    public void sort() {
        flowers.sort(Comparator.comparing(Flower::getName));
    }

    public void outputSAXXML(String path) throws FileNotFoundException, UnsupportedEncodingException, XMLStreamException {
        OutputStream outputStream = new FileOutputStream(new File(path));

        XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
                new OutputStreamWriter(outputStream, "utf-8"));

        out.writeStartElement("flowers");
        out.writeAttribute("xmlns", "http://www.nure.ua");
        out.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        out.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

        for (Flower flower : flowers) {

            out.writeStartElement("flower");

            out.writeStartElement("name");
            out.writeCharacters(flower.getName());
            out.writeEndElement();
            out.writeStartElement("soil");
            out.writeCharacters(flower.getSoil());
            out.writeEndElement();
            out.writeStartElement("origin");
            out.writeCharacters(flower.getOrigin());
            out.writeEndElement();

            out.writeStartElement("visualParameters");

            out.writeStartElement("stemColour");
            out.writeCharacters(flower.getVisualParameters().getStemColour());
            out.writeEndElement();
            out.writeStartElement("leafColour");
            out.writeCharacters(flower.getVisualParameters().getLeafColour());
            out.writeEndElement();
            out.writeStartElement("aveLenFlower");
            out.writeAttribute("measure", flower.getVisualParameters().getAveLenFlowerMeasure());
            out.writeCharacters(flower.getVisualParameters().getAveLenFlower().toString());
            out.writeEndElement();

            out.writeEndElement();

            out.writeStartElement("growingTips");

            out.writeStartElement("tempreture");
            out.writeAttribute("measure", flower.getGrowingTips().getTempretureMeasure());
            out.writeCharacters(flower.getGrowingTips().getTempreture().toString());
            out.writeEndElement();
            out.writeEmptyElement("lighting");
            out.writeAttribute("lightRequiring", flower.getGrowingTips().getLighting());

            out.writeStartElement("watering");
            out.writeAttribute("measure", flower.getGrowingTips().getWateringMeasure());
            out.writeCharacters(flower.getGrowingTips().getWatering().toString());
            out.writeEndElement();

            out.writeEndElement();

            out.writeStartElement("multiplying");
            out.writeCharacters(flower.getMultiplying());
            out.writeEndElement();

            out.writeEndElement();
        }


        out.writeEndElement();
        out.writeEndDocument();

        out.close();
    }

    // PLACE YOUR CODE HERE


}