package com.epam.rd.java.basic.task8.model;

import java.math.BigDecimal;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private Integer aveLenFlower;
    private String aveLenFlowerMeasure;

    public VisualParameters() {
    }

    public VisualParameters(String stemColour, String leafColour, Integer aveLenFlower, String aveLenFlowerMeasure) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
        this.aveLenFlowerMeasure = aveLenFlowerMeasure;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public Integer getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(Integer aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public String getAveLenFlowerMeasure() {
        return aveLenFlowerMeasure;
    }

    public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
        this.aveLenFlowerMeasure = aveLenFlowerMeasure;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower='" + aveLenFlower + '\'' +
                '}';
    }

    boolean isFull() {
        return (stemColour != null && leafColour != null
                && aveLenFlower != null && aveLenFlowerMeasure != null);

    }
}