package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.GrowingTips;
import com.epam.rd.java.basic.task8.model.VisualParameters;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;
    private static List<Flower> flowers = new ArrayList<>();

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(xmlFileName));

        NodeList elements = document.getElementsByTagName("flower");


        for (int i = 0; i < elements.getLength(); i++) {
            NodeList nodes = elements.item(i).getChildNodes();

            if (getFlowerFromNode(nodes) != null)
                flowers.add(getFlowerFromNode(nodes));


            //  System.out.println( node.getTextContent());;
            //  String name = elements.item(i).getTextContent();

        }

        for (Flower flower : flowers)
            System.out.println(flower);
    }

    public Flower getFlowerFromNode(NodeList nodes) {

        Flower flower = new Flower();
        GrowingTips growingTips = new GrowingTips();
        VisualParameters visualParameters = new VisualParameters();

        for (int j = 0; j < nodes.getLength(); j++) {
            switch (nodes.item(j).getNodeName()) {
                case "name":
                    //  System.out.println(nodes.item(j).getTextContent());
                    flower.setName(nodes.item(j).getTextContent());
                    break;
                case "soil":
                    //    System.out.println(nodes.item(j).getTextContent());
                    flower.setSoil(nodes.item(j).getTextContent());
                    break;
                case "origin":
                    //    System.out.println(nodes.item(j).getTextContent());
                    flower.setOrigin(nodes.item(j).getTextContent());
                    break;
                case "multiplying":
                    //    System.out.println(nodes.item(j).getTextContent());
                    flower.setMultiplying(nodes.item(j).getTextContent());
                    break;
                case "growingTips":

                    for (int i = 0; i < nodes.item(j).getChildNodes().getLength(); i++) {
                        Node node = nodes.item(j).getChildNodes().item(i);

                        if (node.getNodeName().equalsIgnoreCase("tempreture")) {
                            growingTips.setTempreture(Integer.valueOf(node.getTextContent()));
                            growingTips.setTempretureMeasure(node.getAttributes().getNamedItem("measure").getTextContent());

                            //    System.out.println(node.getAttributes().getNamedItem("measure").getTextContent());
                            //     System.out.println(node.getTextContent());
                        }
                        if (node.getNodeName().equalsIgnoreCase("lighting")) {
                            growingTips.setLighting(node.getAttributes().getNamedItem("lightRequiring").getTextContent());

                            //       System.out.println(node.getAttributes().getNamedItem("lightRequiring").getTextContent());
                        }
                        if (node.getNodeName().equalsIgnoreCase("watering")) {
                            growingTips.setWatering(Integer.valueOf(node.getTextContent()));
                            growingTips.setWateringMeasure(node.getAttributes().getNamedItem("measure").getTextContent());

                            //       System.out.println(node.getAttributes().getNamedItem("measure").getTextContent());
                            //      System.out.println(node.getTextContent());
                        }
                    }
                    break;
                case "visualParameters":
                    for (int i = 0; i < nodes.item(j).getChildNodes().getLength(); i++) {
                        Node node = nodes.item(j).getChildNodes().item(i);

                        if (node.getNodeName().equalsIgnoreCase("stemColour")) {
                            visualParameters.setStemColour(node.getTextContent());
                            //   System.out.println(node.getTextContent());
                        }
                        if (node.getNodeName().equalsIgnoreCase("leafColour")) {
                            visualParameters.setLeafColour(node.getTextContent());
                            //       System.out.println(node.getTextContent());
                        }
                        if (node.getNodeName().equalsIgnoreCase("aveLenFlower")) {

                            visualParameters.setAveLenFlower(Integer.valueOf(node.getTextContent()));
                            visualParameters.setAveLenFlowerMeasure(node.getAttributes().getNamedItem("measure").getTextContent());

                            //         System.out.println(node.getAttributes().getNamedItem("measure").getTextContent());
                            //        System.out.println(node.getTextContent());
                        }
                    }
                    break;
            }


        }

        flower.setGrowingTips(growingTips);
        flower.setVisualParameters(visualParameters);

        if (flower.isFull())
            return flower;
        return null;
    }

    public void sort() {
        flowers.sort(Comparator.comparingInt(o -> o.getVisualParameters().getAveLenFlower()));
    }
    // PLACE YOUR CODE HERE

    public void outputDom(String path) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("flowers");
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        doc.appendChild(rootElement);

        for (Flower flower : flowers) {
            Element flowerElement = doc.createElement("flower");
            rootElement.appendChild(flowerElement);

            Element nameElement = doc.createElement("name");
            flowerElement.appendChild(nameElement);
            nameElement.setTextContent(flower.getName());

            Element soilElement = doc.createElement("soil");
            soilElement.setTextContent(flower.getSoil());
            flowerElement.appendChild(soilElement);

            Element originElement = doc.createElement("origin");
            originElement.setTextContent(flower.getOrigin());
            flowerElement.appendChild(originElement);

            //-----
            Element visualParametersElement = doc.createElement("visualParameters");
            flowerElement.appendChild(visualParametersElement);


            Element stemColourElement = doc.createElement("stemColour");
            stemColourElement.setTextContent(flower.getVisualParameters().getStemColour());
            visualParametersElement.appendChild(stemColourElement);
            Element leafColourElement = doc.createElement("leafColour");
            leafColourElement.setTextContent(flower.getVisualParameters().getLeafColour());
            visualParametersElement.appendChild(leafColourElement);
            Element aveLenFlowerElement = doc.createElement("aveLenFlower");
            aveLenFlowerElement.setTextContent(flower.getVisualParameters().getAveLenFlower().toString());
            aveLenFlowerElement.setAttribute("measure", flower.getVisualParameters().getAveLenFlowerMeasure());
            visualParametersElement.appendChild(aveLenFlowerElement);
            //-----

            //-----
            Element growingTipsElement = doc.createElement("growingTips");
            flowerElement.appendChild(growingTipsElement);


            Element tempretureElement = doc.createElement("tempreture");
            tempretureElement.setTextContent(flower.getGrowingTips().getTempreture().toString());
            tempretureElement.setAttribute("measure", flower.getGrowingTips().getTempretureMeasure());
            growingTipsElement.appendChild(tempretureElement);

            Element lightingElement = doc.createElement("lighting");
            lightingElement.setAttribute("lightRequiring", flower.getGrowingTips().getLighting());
            growingTipsElement.appendChild(lightingElement);

            Element wateringElement = doc.createElement("watering");
            wateringElement.setTextContent(flower.getGrowingTips().getWatering().toString());
            wateringElement.setAttribute("measure", flower.getGrowingTips().getWateringMeasure());
            growingTipsElement.appendChild(wateringElement);
            //-----
            Element multiplyingElement = doc.createElement("multiplying");
            multiplyingElement.setTextContent(flower.getMultiplying());
            flowerElement.appendChild(multiplyingElement);

        }

        try {
            writeXml(doc, path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // write doc to output stream
    private void writeXml(Document doc,
                          String path) throws IOException, TransformerException {

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        // pretty print
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);

        StreamResult result;
        try (FileWriter writer = new FileWriter(new File(path))) {
            result = new StreamResult(writer);
            transformer.transform(source, result);
        }




    }

}
