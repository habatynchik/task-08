package com.epam.rd.java.basic.task8.model;

public class GrowingTips {
    private Integer tempreture;
    private String tempretureMeasure;
    private String lighting;
    private Integer watering;
    private String wateringMeasure;

    public GrowingTips() {
    }

    public GrowingTips(Integer tempreture, String tempretureMeasure, String lighting, Integer watering, String wateringMeasure) {
        this.tempreture = tempreture;
        this.tempretureMeasure = tempretureMeasure;
        this.lighting = lighting;
        this.watering = watering;
        this.wateringMeasure = wateringMeasure;
    }

    public Integer getTempreture() {
        return tempreture;
    }

    public void setTempreture(Integer tempreture) {
        this.tempreture = tempreture;
    }

    public String getTempretureMeasure() {
        return tempretureMeasure;
    }

    public void setTempretureMeasure(String tempretureMeasure) {
        this.tempretureMeasure = tempretureMeasure;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public Integer getWatering() {
        return watering;
    }

    public void setWatering(Integer watering) {
        this.watering = watering;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture='" + tempreture + '\'' +
                ", tempretureMeasure='" + tempretureMeasure + '\'' +
                ", lighting='" + lighting + '\'' +
                ", watering='" + watering + '\'' +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                '}';
    }

    boolean isFull() {
        return (tempreture != null && tempretureMeasure != null
                && lighting != null && watering != null &&
                wateringMeasure != null);

    }
}
