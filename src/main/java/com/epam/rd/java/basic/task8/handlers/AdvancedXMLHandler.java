package com.epam.rd.java.basic.task8.handlers;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class AdvancedXMLHandler extends DefaultHandler {

    private List<Flower> flowers = new ArrayList<>();
    private Flower flower = new Flower();
    private String lastElementName;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        lastElementName = qName;


    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String information = new String(ch, start, length);

        information = information.replace("\n", "").trim();

        if (!information.isEmpty()) {

            if (lastElementName.equals("name"))
                flower.setName(information);
            if (lastElementName.equals("soil"))
                flower.setSoil(information);
            if (lastElementName.equals("origin"))
                flower.setOrigin(information);
            if (lastElementName.equals("stemColour"))
                flower.setOrigin(information);

        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {


        if (flower.getName() != null && flower.getOrigin() != null && flower.getSoil() != null) {
            flowers.add(flower);
            flower = new Flower();
        }
    }

    public List<Flower> getFlowers() {
        return flowers;
    }
}
