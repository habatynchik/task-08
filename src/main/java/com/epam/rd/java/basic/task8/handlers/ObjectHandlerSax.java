package com.epam.rd.java.basic.task8.handlers;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.GrowingTips;
import com.epam.rd.java.basic.task8.model.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ObjectHandlerSax extends DefaultHandler {

    private StringBuilder currentValue = new StringBuilder();
    List<Flower> result;
    Flower currentFlower;
    GrowingTips currentGrowingTips;
    VisualParameters currentVisualParameters;

    public List<Flower> getResult() {
        return result;
    }

    @Override
    public void startDocument() {
        result = new ArrayList<>();
    }

    @Override
    public void startElement(
            String uri,
            String localName,
            String qName,
            Attributes attributes) {

        // reset the tag value
        currentValue.setLength(0);

        // start of loop
        if (qName.equalsIgnoreCase("flower")) {

            currentFlower = new Flower();
            currentVisualParameters = new VisualParameters();
            currentGrowingTips = new GrowingTips();

        }

        if (qName.equalsIgnoreCase("aveLenFlower")) {
            String measure = attributes.getValue("measure");
            currentVisualParameters.setAveLenFlowerMeasure(measure);
        }

        if (qName.equalsIgnoreCase("tempreture")) {
            String measure = attributes.getValue("measure");
            currentGrowingTips.setTempretureMeasure(measure);
        }

        if (qName.equalsIgnoreCase("watering")) {
            String measure = attributes.getValue("measure");
            currentGrowingTips.setWateringMeasure(measure);
        }

        if (qName.equalsIgnoreCase("lighting")) {
            String measure = attributes.getValue("lightRequiring");
            currentGrowingTips.setLighting(measure);
        }

    }

    public void endElement(String uri,
                           String localName,
                           String qName) {

        if (qName.equalsIgnoreCase("name")) {
            currentFlower.setName(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("soil")) {
            currentFlower.setSoil(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("origin")) {
            currentFlower.setOrigin(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("stemColour")) {
            currentVisualParameters.setStemColour(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("leafColour")) {
            currentVisualParameters.setLeafColour(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("aveLenFlower")) {
            currentVisualParameters.setAveLenFlower(Integer.valueOf(currentValue.toString()));
        }

        if (qName.equalsIgnoreCase("tempreture")) {
            currentGrowingTips.setTempreture(Integer.valueOf(currentValue.toString()));
        }

        if (qName.equalsIgnoreCase("watering")) {
            currentGrowingTips.setWatering(Integer.valueOf(currentValue.toString()));
        }

        if (qName.equalsIgnoreCase("multiplying")) {
            currentFlower.setMultiplying(currentValue.toString());
        }

        // end of loop
        if (qName.equalsIgnoreCase("flower")) {
            currentFlower.setVisualParameters(currentVisualParameters);
            currentFlower.setGrowingTips(currentGrowingTips);
            if (currentFlower.isFull())
                result.add(currentFlower);
        }

    }


    public void characters(char ch[], int start, int length) {
        currentValue.append(ch, start, length);
    }
}
